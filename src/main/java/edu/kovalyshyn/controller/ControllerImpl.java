package edu.kovalyshyn.controller;

import edu.kovalyshyn.model.Model;
import edu.kovalyshyn.model.ModelImpl;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }

    @Override
    public void parserJson() {
        model.parserJson();
    }

    @Override
    public void sortedData() {
        model.sortedData();
    }
}

