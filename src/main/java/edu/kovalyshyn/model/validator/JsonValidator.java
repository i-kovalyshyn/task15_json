package edu.kovalyshyn.model.validator;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.File;
import java.io.IOException;

import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class JsonValidator {
    public static boolean isValid(String jsonPath, String schemaPath)
            throws IOException, ProcessingException{
         JsonNode schemaNode = JsonLoader.fromFile(new File(schemaPath));
         JsonNode jsonNode = JsonLoader.fromFile(new File(jsonPath));
        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonSchema schema = factory.getJsonSchema(schemaNode);
        return schema.validInstance(jsonNode);
    }
}
