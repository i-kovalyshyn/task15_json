package edu.kovalyshyn.model.gson;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import edu.kovalyshyn.model.bank.Banks;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

public class GsonParser {
    private Gson gson = new Gson();

    public Banks[] paseBanks(String jsonFilePath) throws FileNotFoundException{
        JsonReader reader = new JsonReader(
                new InputStreamReader(
                        new FileInputStream(jsonFilePath)
                )
        );
        reader.setLenient(true);
        return gson.fromJson(reader, Banks[].class);
    }
}
