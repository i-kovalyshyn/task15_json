package edu.kovalyshyn.model;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import edu.kovalyshyn.model.bank.Banks;
import edu.kovalyshyn.model.comparator.BanksComparator;
import edu.kovalyshyn.model.gson.GsonParser;
import edu.kovalyshyn.model.jackson.JacksonParser;
import edu.kovalyshyn.model.validator.JsonValidator;
import edu.kovalyshyn.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ModelImpl implements Model {
    private static Logger log = LogManager.getLogger(MyView.class);
    private GsonParser gsonParser = new GsonParser();
    private JacksonParser jacksonParser = new JacksonParser();
    private String jsonFilePath = "/home/ihor/Desktop/EPAM/task15_json/src/main/resources/banks.json";
    private String jsonSchemaPath = "/home/ihor/Desktop/EPAM/task15_json/src/main/resources/banksSchema.json";


    @Override
    public void parserJson() {
        try {
            if (!validateJson()) {
                log.error("Invalid json file");
            }else {
                System.out.println("Gson parser:");
                Arrays.asList(gsonParser.paseBanks(jsonFilePath))
                        .forEach(System.out::println);
                System.out.println("Jackson parser");
                Arrays.asList(jacksonParser.parseBank(jsonFilePath))
                        .forEach(System.out::println);
            }
        }catch (IOException | ProcessingException e){
            e.printStackTrace();
        }

    }

    private boolean validateJson() throws IOException, ProcessingException{
        return JsonValidator.isValid(jsonFilePath, jsonSchemaPath);
    }

    @Override
    public void sortedData() {
try{
    List<Banks> banks = Arrays.asList(jacksonParser.parseBank(jsonFilePath));
    banks.sort(new BanksComparator());
    banks.forEach(System.out::println);
}catch (IOException e){
    e.printStackTrace();
}
    }
}

