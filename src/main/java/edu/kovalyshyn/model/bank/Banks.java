package edu.kovalyshyn.model.bank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Banks {
    private int bankNo;
    private String name;
    private String country;
    private String depositor;
    private int accountId;
    private int amountOnDeposit;
    private double profitability;
    private int timeConstraints;
    private Type type;


}
