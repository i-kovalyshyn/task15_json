package edu.kovalyshyn.model.bank;

public enum Type {
    SAVING("ощадний"),
    ACCUMULATIVE("накопичувальний"),
    ESTIMATED("розрахунковий");
    private boolean status;

    Type(String typeName) {
        this.status = false;
    }

    public boolean status() {
        return status;
    }

    public void setStatus(boolean stat) {
        status = stat;
    }

    @Override
    public String toString() {
        return "Type{" + getClass().getTypeName()+
                "status=" + status +
                '}';
    }
}
