package edu.kovalyshyn.model.comparator;

import edu.kovalyshyn.model.bank.Banks;

import java.util.Comparator;

public class BanksComparator implements Comparator<Banks> {
    @Override
    public int compare(Banks o1, Banks o2) {
        return o1.getName().length()-o2.getName().length();
    }
}
