package edu.kovalyshyn.model.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kovalyshyn.model.bank.Banks;

import java.io.File;
import java.io.IOException;

public class JacksonParser {
    private ObjectMapper objectMapper = new ObjectMapper();

    public Banks[] parseBank(String jsonFilePath) throws IOException{
        return objectMapper.readValue(new File(jsonFilePath), Banks[].class);
    }
}
