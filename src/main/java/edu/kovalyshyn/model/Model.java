package edu.kovalyshyn.model;

public interface Model {
   void parserJson();
   void sortedData();
}
