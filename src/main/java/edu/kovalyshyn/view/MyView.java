package edu.kovalyshyn.view;

import edu.kovalyshyn.controller.ControllerImpl;
import edu.kovalyshyn.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(MyView.class);


    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 -parsed json ");
        menu.put("2", " 2 -sorted Data");
        menu.put("Q", " Q - EXIT");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
        controller.parserJson();

    }

    private void pressButton2() {
        controller.sortedData();
    }


    private void outputMenu() {
        System.out.println("\n MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                log.info("-EXIT- ");
            }
        } while (!keyMenu.equals("Q"));
    }
}
